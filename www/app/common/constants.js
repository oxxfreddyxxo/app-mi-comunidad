(function () {
    'use strict';
    angular
        .module('constants', [])
        //.constant('URL_API_CONSTANTS', 'http://localhost:1337/')
        .constant('URL_API_CONSTANTS', 'https://mi-comunidad.herokuapp.com/')
        .constant('FACEBOOK_CONSTANT', 'facebook')
        .constant('TWITTER_CONSTANT', 'twitter')
        .constant('GOOGLE_CONSTANT', 'google')

})();
