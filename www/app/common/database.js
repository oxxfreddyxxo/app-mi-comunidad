(function () {
    angular
        .module('factories', ['constants'])
        .factory('usuarioFactorieDatabase', usuarioFactorieDatabase);

    function usuarioFactorieDatabase($q) {
        var _db;
        var _usuario;

        return {
            initDB: initDB,
            agregarUsuario: agregarUsuario,
            getUsuario: getUsuario
        };

        function initDB() {
            try{
                _db = new PouchDB('usuario');
            }catch(e){
                alert("initDB");
                alert(e);
            }

        }

        function agregarUsuario(usuario) {
            var deferred = $q.defer();
            deferred.resolve(_db.post(usuario));
            return deferred.promise;
        };

        function getUsuario() {
            try{
                var deferred = $q.defer();

                if (!_usuario) {
                    deferred.resolve(_db.allDocs({include_docs: true})
                        .then(function (docs) {

                            // Each row has a .doc object and we just want to send an
                            // array of birthday objects back to the calling controller,
                            // so let's map the array to contain just the .doc objects.
                            _usuario = docs.rows.map(function (row) {
                                return row.doc;
                            });

                            // Listen for changes on the database.
                            _db.changes({live: true, since: 'now', include_docs: true})
                                .on('change', onDatabaseChange);

                            return _usuario;
                        }));
                } else {
                    // Return cached data as a promise
                    deferred.resolve(_usuario);
                }

                return deferred.promise;
            }catch(e){
                alert("Consultar");
                alert(e);
            }
        };

        function onDatabaseChange(change) {
            var index = findIndex(_usuario, change.id);
            var birthday = _usuario[index];

            if (change.deleted) {
                if (birthday) {
                    _usuario.splice(index, 1); // delete
                }
            } else {
                if (birthday && birthday._id === change.id) {
                    _usuario[index] = change.doc; // update
                } else {
                    _usuario.splice(index, 0, change.doc) // insert
                }
            }
        }

        function findIndex(array, id) {
            var low = 0, high = array.length, mid;
            while (low < high) {
                mid = (low + high) >>> 1;
                array[mid]._id < id ? low = mid + 1 : high = mid
            }
            return low;
        }
    }

})();