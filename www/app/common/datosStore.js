angular.module('service')
    .service('usuarioStore',usuarioStore)
    .service('grupoStore',grupoStore)
    .service('redSocialStore',redSocialStore)
    .service('configStore',configStore)
    .service('pushStore',pushStore);

    function grupoStore(){
        var grupo = {
            id:'',
            grupo_nombre: '',
            grupo_direccion: '',
            grupo_numero: null,
            comuna: null,
            grupo_costo: 1,
            usuario: 1,
            plan_cuadrante:null,
            estado:"556e5c81e4b0a850c4d4d1f7",
            tipo_grupo: 1
        };

        this.setGrupo = function (g) {
            grupo = g;
        };

        this.getGrupo = function () {
            return grupo;
        };
    };

    function usuarioStore() {
        var usuario = {
            id:'', //id de heroku
            usu_nombre: '',
            usu_correo: '',
            usu_celular: null,
            usu_direccion: '',
            usu_numero: null,
            usu_depto: null,
            usu_clave:'',
            red_social:null
        };

        var usuStatus =  {
            error: false,
                registrado: false,
                mensaje: ''
        };

        this.setUsuStatus = function (status) {
            usuStatus = status;
        };

        this.getUsuStatus = function () {
            return usuStatus;
        };

        this.setUsuario = function (user) {
            usuario = user;
        };

        this.getUsuario = function () {
            return usuario;
        };

    };

    function pushStore(){

        var push = {
            token:""
        }


        this.setPush = function (p) {
            push = p;
        };

        this.getPush = function () {
            return push;
        };
    }

    function redSocialStore(){
        var redSocial = {
            rs_tipo:'',
            rs_id:'',
            rs_link:'',
            _id:''
        };

        this.setRedSocial = function (r) {
            redSocial = r;
        };

        this.getRedSocial = function () {
            return redSocial;
        };
    };

    function configStore(){
        var config = {
            version:0,
            primeraVez : false
        };

        this.setConfig = function (c) {
            config = c;
        };

        this.getConfig = function () {
            return config;
        };

    };
