(function () {
    angular
        .module('factories', [])
        .factory('usuarioFactorie', usuarioFactorie)
        .factory('almacenFactorie', almacenFactorie)
        .factory('grupoFactory', grupoFactory)
        .factory('configFactory', configFactory);

    function almacenFactorie($http) {
        return {
            getComunas: getComunas
        };

        function getComunas() {
            return $http({
                url: "almacen/comunas.json"
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });
        }
    }

    function usuarioFactorie($http, usuarioStore, URL_API_CONSTANTS, redSocialStore) {

        return {
            registrar: registrar,
            login: login
        };

        function registrar() {
            return $http({
                url: URL_API_CONSTANTS + "usuario/nuevoUsuario",
                method: "POST",
                data: {"usuario": usuarioStore.getUsuario(), "red_social": redSocialStore.getRedSocial()}
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });

        }

        function login() {
            return $http({
                url: URL_API_CONSTANTS + "usuario/login",
                method: "POST"
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });

        }

    }

    function grupoFactory($http, URL_API_CONSTANTS, grupoStore) {
        return {
            crearGrupo: crearGrupo
        };

        function crearGrupo() {
            return $http({
                url: URL_API_CONSTANTS + "grupo/crearGrupo",
                method: "POST",
                data: grupoStore.getGrupo()
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });
        }
    }

    function configFactory($http, URL_API_CONSTANTS) {
        return {
            cargaInicial: cargaInicial,
            getVersion: getVersion
        };

        function cargaInicial() {
            return $http({
                url: URL_API_CONSTANTS + "configuraciones/inicial",
                method: "POST"
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });
        }

        function getVersion() {
            return $http({
                url: URL_API_CONSTANTS + "configuraciones/getVersion",
                method: "POST"
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });
        }
    }

})();