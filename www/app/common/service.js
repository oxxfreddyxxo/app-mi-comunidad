angular.module('service', [])
    .service('usuarioService', usuarioService)
    .service('loading', loading)
    .service('push', push);

function usuarioService(usuarioFactorie,
                        usuarioStore,
                        usuarioFactorieSqlLite,
                        redSocialStore,
                        $http,
                        $rootScope,
                        loading,
                        push) {
    return {
        crearUsuario: crearUsuario,
        login: login
    }

    function crearUsuario() {
        var respuesta = {
            error: false,
            path: "",
            msjerror: ""
        };

        //Se registra en heroku
        return usuarioFactorie.registrar().then(function (data) {
            var usuario = usuarioStore.getUsuario();
            var redSocial = redSocialStore.getRedSocial();

            usuario.id = data.id;
            redSocial._id = data.red_social;

            usuarioStore.setUsuario(usuario);
            redSocialStore.setRedSocial(redSocial);

            //GUARDA EN SQLITE
            return usuarioFactorieSqlLite.registrar().then(function () {
                loading.hide();
                if (usuarioStore.getUsuStatus().registrado) {
                    //Esta validacion es para probar en desarrollo
                    if (window.pushNotification !== undefined) {
                        push.registrarDispositivo();

                        $rootScope.$on('$cordovaPush:tokenReceived', function (event, data) {
                            var push = {
                                "token": data.token
                            };

                            pushStore.setPush(push);

                            respuesta.path = "/app/home";
                        });
                    } else {
                        respuesta.path = "/app/home";
                    }

                } else {
                    respuesta.path = '/platform';
                }

                return respuesta;

            }, function (e) {
                var mensaje = {
                    texto: "Lo sentimos ha ocurrido un error",
                    estado: 'error'
                };
                modal.openError(mensaje);

                respuesta.error = true;
                return respuesta;
            });

        }, function (e) {
            var mensaje = {
                texto: "Ha ocurrido un error. Vuelva a intentarlo",
                estado: 'error'
            };

            modal.openError(mensaje);
            respuesta.error = true;
            return respuesta;
        });
    }

    function login(correo, clave) {
        loading.show();

        $http.defaults.headers.post['USUARIO_CORREO'] = correo;
        $http.defaults.headers.post['USUARIO_CLAVE'] = clave;

        var respuesta = {
            error: false,
            path: "",
            msjerror: ""
        };

        var usuario = usuarioStore.getUsuario();

        //Valida en heroku
        return usuarioFactorie.login().then(function (data) {
            if (data.autorizacion) {
                usuario.id = data.id;
                usuario.usu_nombre = data.usu_nombre;
                usuario.usu_celular = data.usu_celular;
                usuario.usu_direccion = data.usu_direccion;
                usuario.usu_numero = data.usu_numero;
                usuario.usu_depto = data.usu_depto;
                usuario.usu_clave = clave;
                usuario.usu_correo = correo;

                usuarioStore.setUsuario(usuario);

                //GUARDA EN SQLITE
                return usuarioFactorieSqlLite.registrar().then(function () {
                    if (usuarioStore.getUsuStatus().registrado) {

                        //Esta validacion es para probar en desarrollo
                        if (window.pushNotification !== undefined) {
                            push.registrarDispositivo();

                            $rootScope.$on('$cordovaPush:tokenReceived', function (event, data) {
                                var push = {
                                    "token": data.token
                                };

                                pushStore.setPush(push);

                                respuesta.path = "/app/home";
                            });
                        } else {
                            respuesta.path = "/app/home";
                        }

                    } else {
                        respuesta.path = "/platform";
                    }

                    loading.hide();
                    return respuesta;

                }, function (e) {
                    loading.hide();
                    return respuesta;
                });
            } else {

                loading.hide();

                respuesta.error = true;
                respuesta.msjerror = "Acceso denegado";

                return respuesta;
            }
        }, function (e) {
            respuesta.error = true;
            respuesta.msjerror = "Ha ocurrido un error. Intentelo más tarde";

            loading.hide();
            return respuesta;
        });
    }
}


function loading($ionicLoading) {
    return {
        show: show,
        hide: hide
    }

    function show() {
        $ionicLoading.show({
            template: '<div class="spinner-block"><ion-spinner></ion-spinner></div>'
        });
    }

    function hide() {
        $ionicLoading.hide();
    }
}

function push($ionicUser, $ionicPush, usuarioStore) {

    return {
        registrarDispositivo: registrarDispositivo
    }

    function registrarDispositivo() {

        var user_phone = $ionicUser.get();
        var usuario_heroku = usuarioStore.getUsuario();
        user_phone.user_id = usuario_heroku.id;

        // Metadata
        angular.extend(user_phone, {
            name: usuario_heroku.usu_nombre
        });


        $ionicUser.identify(user_phone).then(function () {
            console.log('Identified user ' + user_phone.name + '\n ID ' + user_phone.user_id);
        });


        $ionicPush.register({
            canShowAlert: true, //Can pushes show an alert on your screen?
            canSetBadge: true, //Can pushes update app icon badges?
            canPlaySound: true, //Can notifications play a sound?
            canRunActionsOnWake: true, //Can run actions outside the app,
            onNotification: function (notification) {
                // Handle new push notifications here
                return true;
            }
        });

    }
}