(function () {
    angular
        .module('factories')
        .factory('bdFactorySqlLite', bdFactorySqlLite)
        .factory('usuarioFactorieSqlLite', usuarioFactorieSqlLite)
        .factory('configFactorieSqlLite', configFactorieSqlLite);


    var app = {};
    app.db = null;


    function bdFactorySqlLite() {
        return {
            init: init
        };

        function init() {
            if (window.sqlitePlugin !== undefined) {
                app.db = window.sqlitePlugin.openDatabase("security.db");
            } else {
                // For debugging in simulator fallback to native SQL Lite
                app.db = window.openDatabase("security.db", "1.0", "Cordova Demo", 200000);
            }

            app.db.transaction(function (tx) {
                var query = "CREATE TABLE IF NOT EXISTS usuario (usu_id text, usu_nombre text, usu_correo text, ";
                query = query + "usu_celular text, usu_direccion text, usu_numero integer, usu_depto integer, usu_clave); ";
                tx.executeSql(query, []);

                query = "CREATE TABLE IF NOT EXISTS configuraciones (config_verion text); ";
                tx.executeSql(query, []);

                query = "CREATE TABLE IF NOT EXISTS estado (estado_id text, estado_nombre text); ";
                tx.executeSql(query, []);

                query = "CREATE TABLE IF NOT EXISTS tipo_evento (te_id text,te_nombre text); ";
                tx.executeSql(query, []);

                query = "CREATE TABLE IF NOT EXISTS tipo_grupo (tg_id text, tg_nombre text); ";
                tx.executeSql(query, []);

                query = "CREATE TABLE IF NOT EXISTS comunas (comuna_id text, comuna_nombre text); ";
                tx.executeSql(query, []);

                query = "CREATE TABLE IF NOT EXISTS red_social (rs_tipo text, rs_id text, rs_link, _id text); ";
                tx.executeSql(query, []);
            });
        }

    }

    function usuarioFactorieSqlLite($q, usuarioStore, redSocialStore) {
        return {
            registrar: registrar,
            getConfig: getConfig,
            getUsuario: getUsuario
        };

        function registrar() {
            var q = $q.defer();

            var queryUsu = "INSERT INTO usuario (usu_id, usu_nombre, usu_correo, usu_celular, usu_clave) " +
                " VALUES (?,?,?,?,?)";

            var user = usuarioStore.getUsuario();
            var status = usuarioStore.getUsuStatus();
            var redSocial = redSocialStore.getRedSocial();


            var onSuccess = function (tx, res) {
                status.registrado = true;
                q.resolve(true);
            };

            var onError = function (tx, e) {
                status.error = true;
                status.mensaje = e;
                q.reject(e);
            };

            app.db.transaction(function (tx) {
                tx.executeSql(queryUsu,
                    [user.id, user.usu_nombre, user.usu_correo,
                        user.usu_celular, user.usu_clave],
                    onSuccess,
                    onError);
            });

            //Valido si tiene red social
            if(redSocial.rs_id){
                var queryRed = "INSERT INTO red_social (rs_tipo, rs_id, rs_link, _id) " +
                    " VALUES (?,?,?,?)";

                app.db.transaction(function (tx) {
                    tx.executeSql(queryRed,
                        [redSocial.rs_tipo, redSocial.rs_id, redSocial.rs_link,
                            redSocial._id],
                        onSuccess,
                        onError);
                });
            }

            return q.promise;
        }

        function getConfig() {
            var q = $q.defer();
            var query = "SELECT * FROM configuraciones";

            var configuraciones = {
                config_verion: 0
            };

            var getData = function (tx, res) {
                if (res.rows.length > 0) {
                    configuraciones.config_verion = res.rows.item(0).config_verion;
                }

                q.resolve(configuraciones);
            };

            var onError = function (tx, e) {
                q.reject(e);
            };


            app.db.transaction(function (tx) {
                tx.executeSql(query, [],
                    getData,
                    onError);
            });


            return q.promise;
        }

        function getUsuario() {

            var q = $q.defer();
            var query = "SELECT * FROM usuario";

            var user = usuarioStore.getUsuario();
            var status = usuarioStore.getUsuStatus();

            var getUsu = function (tx, res) {
                if (res.rows.length > 0) {
                    status.registrado = true;
                    user.usu_nombre = res.rows.item(0).usu_nombre;
                    user.usu_correo = res.rows.item(0).usu_correo;
                    user.usu_clave = res.rows.item(0).usu_clave;
                    user.id = res.rows.item(0).usu_id;
                }

                q.resolve(status);
            };

            var onError = function (tx, e) {
                status.error = true;
                status.mensaje = e;
                q.reject(false);
            };


            app.db.transaction(function (tx) {
                tx.executeSql(query, [],
                    getUsu,
                    onError);
            });

            usuarioStore.setUsuario(user);
            usuarioStore.setUsuStatus(status);

            return q.promise;

        }
    }

    function configFactorieSqlLite($q) {
        return {
            cargarComunas: cargarComunas,
            cargarEstado: cargarEstado,
            cargarTipoEvento: cargarTipoEvento,
            cargarTipoGrupo: cargarTipoGrupo,
            cargarConfig: cargarConfig,
            getComunas:getComunas,
            getTipoGrupo:getTipoGrupo
        }



        function cargarComunas(data) {
            var q = $q.defer();

            var onError = function (tx, e) {
                q.reject(e);
                return q.promise;
            };

            app.db.transaction(function (tx) {
                var query = "delete from comunas;";
                tx.executeSql(query,
                    [], null, onError);

                query = "INSERT INTO comunas (comuna_id, comuna_nombre) values (?,?)";
                angular.forEach(data, function (value, key) {
                    tx.executeSql(query,
                        [value.id, value.comuna_nombre], null, onError);
                });

            });



            return q.promise;
        }

        function cargarEstado(data) {
            var q = $q.defer();

            var onError = function (tx, e) {
                q.reject(e);
                return q.promise;
            };

            app.db.transaction(function (tx) {
                var query = "delete from estado;";
                tx.executeSql(query,
                    [], null, onError);

                query = "INSERT INTO estado (estado_id, estado_nombre) values (?,?)";
                angular.forEach(data, function (value, key) {
                    tx.executeSql(query,
                        [value.id, value.estado_nombre], null, onError);
                });

            });

            return q.promise;
        }

        function cargarTipoEvento(data) {
            var q = $q.defer();


            var onError = function (tx, e) {
                q.reject(e);
                return q.promise;
            };


            app.db.transaction(function (tx) {
                var query = "delete from tipo_evento;";
                tx.executeSql(query,
                    [], null, onError);

                query = "INSERT INTO tipo_evento (te_id,te_nombre) values (?,?)";
                angular.forEach(data, function (value, key) {
                    tx.executeSql(query,
                        [value.id, value.te_nombre], null, onError);
                });

            });

            return q.promise;
        }

        function cargarTipoGrupo(data) {
            var q = $q.defer();

            var onError = function (tx, e) {
                q.reject(e);
                return q.promise;
            };
            app.db.transaction(function (tx) {
                var query = "delete from tipo_grupo;";
                tx.executeSql(query,
                    [], null, onError);

                query = "INSERT INTO tipo_grupo (tg_id, tg_nombre) values (?,?)";
                angular.forEach(data, function (value, key) {
                    tx.executeSql(query,
                        [value.id, value.tg_nombre], null, onError);
                });

            });

            return q.promise;
        }

        function cargarConfig(data) {
            var q = $q.defer();

            var onError = function (tx, e) {
                q.reject(e);
                return q.promise;
            };
            app.db.transaction(function (tx) {
                var query = "delete from configuraciones;";
                tx.executeSql(query,
                    [], null, onError);

                query = "INSERT INTO configuraciones (config_verion) values (?)";
                angular.forEach(data, function (value, key) {
                    tx.executeSql(query,
                        [value.config_verion], null, onError);
                });

            });

            return q.promise;
        }

        function getComunas(){
            var q = $q.defer();
            var query = "SELECT * FROM comunas";

            var comunas = [];

            var geComuna = function (tx, res) {
                angular.forEach(res.rows, function (value, key) {
                    var comunasTemp = {
                        comuna_id: value.comuna_id,
                        comuna_nombre:value.comuna_nombre
                    };

                    comunas.push(comunasTemp);

                });

                q.resolve(comunas);
            };

            var onError = function (tx, e) {
                q.reject(e);
            };

            app.db.transaction(function (tx) {
                tx.executeSql(query, [],
                    geComuna,
                    onError);
            });

            return q.promise;
        }

        function getTipoGrupo(){
            var q = $q.defer();
            var query = "SELECT * FROM tipo_grupo";

            var data = [];

            var onSuccess = function (tx, res) {
                angular.forEach(res.rows, function (value, key) {
                    var dataTemp = {
                        tg_id: value.tg_id,
                        tg_nombre:value.tg_nombre
                    };

                    data.push(dataTemp);

                });
                q.resolve(data);
            };

            var onError = function (tx, e) {
                q.reject(e);
            };

            app.db.transaction(function (tx) {
                tx.executeSql(query, [],
                    onSuccess,
                    onError);
            });

            return q.promise;
        }
    }

})
();