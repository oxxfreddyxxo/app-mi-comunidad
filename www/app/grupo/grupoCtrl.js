(function () {
    angular.module('starter')
        .controller('grupoCtrl', grupoCtrl)
        .controller('crearGrupoCtrl', crearGrupoCtrl);


    function grupoCtrl($ionicModal, $rootScope, $scope, $timeout, usuarioStore) {

        /*$scope.pushRegister = function () {
         console.log('Ionic Push: Registering user');

         // Register with the Ionic Push service.  All parameters are optional.
         $ionicPush.register({
         canShowAlert: true, //Can pushes show an alert on your screen?
         canSetBadge: true, //Can pushes update app icon badges?
         canPlaySound: true, //Can notifications play a sound?
         canRunActionsOnWake: true, //Can run actions outside the app,
         onNotification: function (notification) {
         // Handle new push notifications here
         return true;
         }
         });
         };

         ;*/

        //PUSH TEST



        $scope.map = {center: {latitude: -33.457417, longitude: -70.565824}, zoom: 18};
        $scope.marker = {
            id: 0,
            coords: {
                latitude: -33.457490,
                longitude: -70.565381
            },
            icon: "img/block29.png"
        };
        $scope.markerUser = [
            {
                id: 001,
                coords: {
                    latitude: -33.457417,
                    longitude: -70.565824
                }
            },
            {
                id: 002,
                coords: {
                    latitude: -33.457519,
                    longitude: -70.565740
                }
            },
            {
                id: 003,
                coords: {
                    latitude: -33.457611,
                    longitude: -70.565325
                }
            }
        ];
        $scope.users = [
            {
                name: "Mario Reyes",
                adress: "Cristobal Colon 5234",
                avatar: "user1.jpg"

            },
            {
                name: "Camila Illanes",
                adress: "Cristobal Colon 5238",
                avatar: "user8.jpg"

            },
            {
                name: "Dario Perez",
                adress: "Cristobal Colon 5234",
                avatar: "user12.jpg"

            },
            {
                name: "Luis Ramos",
                adress: "Cristobal Colon 5238",
                avatar: "user128.jpg"

            }
        ]

        $scope.telefonos = [
            {
                titulo: "Plan Cuadrante",
                contacto: "Cabo 1º Alejandro Lillo Navarrete",
                telefono: "9291902"

            },
            {
                titulo: "Jefe Guardias ADT",
                contacto: "Francisco Navarrete",
                telefono: "9458912"

            },
            {
                titulo: "Paz Ciudadana Las Condes",
                contacto: "Luis Penailillo",
                telefono: "9291902"

            }
        ];

        //CUENTA A TRAS

        $scope.counter = 5;

        var mytimeout = null; // the current timeoutID

        // actual timer method, counts down every second, stops on zero
        $scope.onTimeout = function () {
            if ($scope.counter === 0) {
                //alert('0');
                $scope.modal.hide();
                $scope.counter = 5;//reseteamos el contador
                $scope.$broadcast('timer-stopped', 0);
                $timeout.cancel(mytimeout);
                return;

            }
            $scope.counter--;
            mytimeout = $timeout($scope.onTimeout, 1000);
        };

        $scope.startTimer = function () {
            mytimeout = $timeout($scope.onTimeout, 1000);
        };

        // stops and resets the current timer
        $scope.stopTimer = function () {
            $scope.$broadcast('timer-stopped', $scope.counter);
            $scope.counter = 5;
            $timeout.cancel(mytimeout);
        };


        // triggered, when the timer stops, you can do something here, maybe show a visual indicator or vibrate the device
        $scope.$on('timer-stopped', function (event, remaining) {
            if (remaining === 0) {
                console.log('your time ran out!');
            }
        });


        $ionicModal.fromTemplateUrl('my-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
            $scope.startTimer();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
            $scope.stopTimer();
        };

        $ionicModal.fromTemplateUrl('app/grupo/modal-nuevo-numero.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.nuevoNumero = modal;
        });

        $scope.addPhone = function () {
            $scope.nuevoNumero.show();
        };
        $scope.closePhone = function () {
            $scope.nuevoNumero.hide();
        };

        $scope.onSwipeLeft = function () {
            alert('swipe');
        };
        $scope.btnScale = true;
        $scope.onHold = function () {
            $scope.btnScale = !$scope.btnScale;
        };
    };

    function crearGrupoCtrl($scope,
                            configFactorieSqlLite,
                            grupoStore,
                            modal,
                            grupoFactory,
                            usuarioStore,
                            bdFactorySqlLite,
                            loading) {

        bdFactorySqlLite.init();

        $scope.matchComuna = [];
        $scope.visible = true;
        $scope.noMostrar = '';

        $scope.grupo = grupoStore.getGrupo();
        $scope.nombreTipoGrupo = "";

        configFactorieSqlLite.getComunas().then(function (com) {
            $scope.comunas = com;
        }, function (e) {
            var mensaje = {
                texto: "Ha ocurrido un error",
                estado: 'error'
            };

            modal.openError(mensaje);
        });

        configFactorieSqlLite.getTipoGrupo().then(function (data) {
            $scope.tipoGrupo = data;
        }, function (e) {
            var mensaje = {
                texto: "Ha ocurrido un error",
                estado: 'error'
            };

            modal.openError(mensaje);
        });

        $scope.buscarComuna = function () {
            if ($scope.grupo.comuna === '') {
                $scope.matchComuna = [];
                $scope.visible = true;
            } else {
                $scope.visible = false;
                $scope.matchComuna = $scope.comunas.filter(function (data) {
                    if (data.comuna_nombre.toLowerCase().indexOf($scope.grupo.comuna.toLowerCase()) !== -1) return true;
                });
            }
        };

        $scope.seleccionComuna = function (seleccion) {
            $scope.matchComuna = [];
            $scope.grupo.comuna = seleccion.comuna_nombre;
            $scope.visible = true;
        };

        $scope.crearGrupo = function (form) {
            if (form.$valid) {
                loading.show();
                $scope.grupo.usuario = usuarioStore.getUsuario().id;

                if ($scope.nombreTipoGrupo != 'vecinal') {
                    $scope.grupo.grupo_direccion = '';
                    $scope.grupo.grupo_numero = null;
                    $scope.grupo.comuna = null;
                }

                grupoStore.setGrupo($scope.grupo);
                grupoFactory.crearGrupo().then(function (data) {
                    console.log(data);
                    loading.hide();
                });
            }
        };

        $scope.grupoSeleccionado = function (seleccion) {
            $scope.grupo.tipo_grupo = seleccion.tg_id;
            $scope.nombreTipoGrupo = seleccion.tg_nombre.toLowerCase();

        };
    };


})();