(function () {
    angular.module('starter')
        .controller('loginCtrl', loginCtrl)
        .controller('crearCuentaCtrl', crearCuentaCtrl);


    function loginCtrl($scope,
                       $http,
                       usuarioStore,
                       $timeout,
                       modal,
                       ngFB,
                       loading,
                       FACEBOOK_CONSTANT,
                       redSocialStore,
                       push,
                       usuarioService,
                       $location) {

        $scope.usuario = usuarioStore.getUsuario();

        $scope.fbLogin = function () {

            loading.show();
            var path = "";

            ngFB.login({scope: 'email,read_stream,publish_actions'}).then(
                function (response) {
                    if (response.status === 'connected') {
                        ngFB.api({
                            path: '/me'
                        }).then(
                            function (user) {
                                var redSocial = redSocialStore.getRedSocial();

                                $scope.usuario.usu_nombre = user.name;
                                $scope.usuario.usu_correo = user.email;
                                redSocial.rs_tipo = FACEBOOK_CONSTANT;
                                redSocial.rs_id = user.id;
                                redSocial.rs_link = user.link;

                                redSocialStore.setRedSocial(redSocial);
                                usuarioStore.setUsuario($scope.usuario);

                                usuarioService.crearUsuario().then(function (respuesta) {

                                    if (!respuesta.error) {
                                        $location.path(respuesta.path);
                                    }
                                });

                            },
                            function (error) {
                                loading.hide();
                            });

                    } else {
                        loading.hide();
                        var mensaje = {
                            texto: "Ha ocurrido un error. Vuelva a intentarlo",
                            estado: 'error'
                        };

                        modal.openError(mensaje);
                    }
                }, function (e) {
                    console.log(e);
                    loading.hide();
                    var mensaje = {
                        texto: "Ha ocurrido un error. Vuelva a intentarlo",
                        estado: 'error'
                    };

                    modal.openError(mensaje);
                });
        };

        $scope.login = function (form) {
            $scope.errorLogin = false;

            if (form.$valid) {
                usuarioService.login($scope.usuario.usu_correo, $scope.usuario.usu_clave).then(function (respuesta) {
                    if (respuesta.error) {
                        $scope.msjerror = respuesta.msjerror;
                        $scope.errorLogin = respuesta.error;
                        $timeout(function () {
                            $scope.errorLogin = false;
                        }, 1000, true);
                    } else {
                        $location.path(respuesta.path);
                    }
                });

            } else {
                $scope.msjerror = "Complete el formulario";
                $scope.errorLogin = true;
                $timeout(function () {
                    $scope.errorLogin = false;
                }, 1000, true);
            }
        };
    }

    function crearCuentaCtrl($scope,
                             usuarioStore,
                             usuarioFactorie,
                             modal,
                             push,
                             loading,
                             $location,
                             usuarioService) {

        $scope.usuario = usuarioStore.getUsuario();
        var path = "";
        $scope.guardarUsuario = function (form) {
            if (form.$valid) {
                loading.show();
                usuarioStore.setUsuario($scope.usuario);

                usuarioService.crearUsuario().then(function (respuesta) {

                    if (!respuesta.error) {
                        $location.path(respuesta.path);
                    }
                });


            } else {
                var mensaje = {
                    texto: "Debe completar el form",
                    estado: 'error'
                };

                modal.openError(mensaje);
            }
        };

    }

})();