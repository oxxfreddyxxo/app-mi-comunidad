(function () {
    angular.module('service')
        .service('modal', modal);

    function modal($ionicModal) {

        return {
            openError: openError
        };

        function openError(mensaje) {
            var modal = [];


            $ionicModal.fromTemplateUrl('app/mensajes/modal.html', {
                animation: 'slide-in-up'
            }).then(function (modal, $scope) {
                modal = modal;
                modal.scope.mensaje= mensaje;
                modal.show();

                modal.scope.closeModal = function() {
                    modal.scope.modal.hide();
                };

            });

        }
    }

})();