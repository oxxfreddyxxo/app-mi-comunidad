angular.module('starter', ['ionic',
    'constants',
    'factories',
    'service',
    'uiGmapgoogle-maps',
    'ngCordova',
    'ngOpenFB',
    'ionic.service.core',
    'ionic.service.push'])

    .run(function ($ionicPlatform,
                   usuarioStore,
                   $location,
                   usuarioFactorieSqlLite,
                   bdFactorySqlLite,
                   $http,
                   configFactory,
                   configFactorieSqlLite,
                   modal,
                   ngFB,
                   configStore) {

        var config = configStore.getConfig();
        ngFB.init({appId: '851476398259613'});

        $ionicPlatform.ready(function () {

            bdFactorySqlLite.init();


            usuarioFactorieSqlLite.getConfig().then(function (data) {
                config.version = data.config_verion;


                //SI ES 0 ENTONCES ES CARGA INICIAL Y ES LA PRIMERA VEZ QUE EL USUARIO UTILIZA ESTA APP
                if (config.version === 0) {
                    config.primeraVez = true;
                    configStore.setConfig(config);

                    var comunas = [], estado = [], tipo_evento = [], tipo_grupo = [], configuracion = [], ok = true;

                    configFactory.cargaInicial().then(function (data) {
                        comunas = data.comunas;
                        estado = data.estado;
                        tipo_evento = data.tipo_evento;
                        tipo_grupo = data.tipo_grupo;
                        configuracion = data.configuracion;


                        configFactorieSqlLite.cargarComunas(comunas).then(function (data) {
                            console.log("Comunas Creadas");
                        }, function (e) {
                            var mensaje = {
                                texto: "Lo sentimos ha ocurrido un error.",
                                estado: 'error'
                            };

                            modal.openError(mensaje);
                            ok = false;
                        });

                        configFactorieSqlLite.cargarEstado(estado).then(function (data) {
                            console.log("Estados Creados");
                        }, function (e) {
                            var mensaje = {
                                texto: "Lo sentimos ha ocurrido un error.",
                                estado: 'error'
                            };

                            modal.openError(mensaje);
                            ok = false;
                        });

                        configFactorieSqlLite.cargarTipoEvento(tipo_evento).then(function (data) {
                            console.log("Tipo de Eventos Creados");
                        }, function (e) {
                            var mensaje = {
                                texto: "Lo sentimos ha ocurrido un error.",
                                estado: 'error'
                            };

                            modal.openError(mensaje);
                            ok = false;
                        });

                        configFactorieSqlLite.cargarTipoGrupo(tipo_grupo).then(function (data) {
                            console.log("Tipo de Grupos Creados");
                        }, function (e) {
                            var mensaje = {
                                texto: "Lo sentimos ha ocurrido un error.",
                                estado: 'error'
                            };

                            modal.openError(mensaje);
                            ok = false;
                        });

                        configFactorieSqlLite.cargarConfig(configuracion).then(function (data) {
                            console.log("Configuracion creado");
                        }, function (e) {
                            var mensaje = {
                                texto: "Lo sentimos ha ocurrido un error.",
                                estado: 'error'
                            };

                            modal.openError(mensaje);
                            ok = false;
                        });

                        if (ok) {
                            $location.path('/login');
                        }
                    });
                } else {
                    configStore.setConfig(config);
                    usuarioFactorieSqlLite.getUsuario().then(function (data) {
                        if (data.registrado) {
                            $http.defaults.headers.post['USUARIO_CORREO'] = usuarioStore.getUsuario().usu_correo;
                            $http.defaults.headers.post['USUARIO_CLAVE'] = usuarioStore.getUsuario().usu_clave;
                            $location.path('/app/home');

                        } else {
                            $location.path('/login');
                        }
                    });


                }
            });
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicAppProvider) {

        $ionicAppProvider.identify({
            // The App ID for the server
            app_id: '7c6bc715',
            // The API key all services will use for this app
            api_key: '76a229fdec5398c60f1d9a321e189ff2b15201ede4d569a0',
            // Your GCM sender ID/project number (Uncomment if using GCM)
            gcm_id: '505112834760'
            //dev_push: true
        });

        $stateProvider.state('platform', {
            url: "/platform",
            templateUrl: "app/platform/platform.html",
            controller: 'platformCtrl'
        }).state('login', {
            url: "/login",
            templateUrl: "app/login/login.html",
            controller: 'loginCtrl'
        }).state('crear', {
            url: "/crearcuenta",
            templateUrl: "app/login/crear-cuenta.html",
            controller: 'crearCuentaCtrl'
        }).state('app', {
            url: "/app",
            templateUrl: "app/menu/menu.html",
            controller: 'menuCtrl',
            abstract: true
        }).state('app.home', {
            url: "/home",
            title: 'Mi Comunidad',
            views: {
                'menuContent': {
                    templateUrl: "app/grupo/home.html",
                    controller: 'grupoCtrl'
                }
            }
        }).state('app.creargrupo', {
            url: "/creargrupo",
            title: 'Crear Grupo',
            views: {
                'menuContent': {
                    templateUrl: "app/grupo/crear-grupo.html",
                    controller: 'crearGrupoCtrl'
                }
            }
        }).state('app.buscargrupo', {
            url: "/buscargrupo",
            title: 'Buscar Grupo',
            views: {
                'menuContent': {
                    templateUrl: "app/grupo/buscar-grupo.html",
                    controller: 'grupoCtrl'
                }
            }
        }).state('app.migrupo', {
            url: "/migrupo",
            title: 'Mi Grupo',
            views: {
                'menuContent': {
                    templateUrl: "app/grupo/mi-grupo.html",
                    controller: 'grupoCtrl'
                }
            }
        }).state('app.migrupousuarios', {
            url: "/migrupousuarios",
            title: 'Mi Grupo',
            views: {
                'menuContent': {
                    templateUrl: "app/grupo/mi-grupo-usuarios.html",
                    controller: 'grupoCtrl'
                }
            }
        }).state('app.confirmarusuarios', {
            url: "/confirmarusuarios",
            title: 'Mi Grupo',
            views: {
                'menuContent': {
                    templateUrl: "app/grupo/mi-grupo-confirmar-usuarios.html",
                    controller: 'grupoCtrl'
                }
            }
        }).state('app.migrupotelefonos', {
            url: "/migrupotelefonos",
            title: 'Mi Grupo',
            views: {
                'menuContent': {
                    templateUrl: "app/grupo/mi-grupo-telefonos.html",
                    controller: 'grupoCtrl'
                }
            }
        }).state('app.mensaje', {
            url: "/mensaje",
            title: 'mensaje',
            views: {
                'menuContent': {
                    templateUrl: "app/mensajes/mensaje.html",
                    controller: 'grupoCtrl'
                }
            }
        })

    });
